# OneMillionPixelWaves

[OneMillionPixelWaves](https://onemillionpixelwaves.com) A blockchain / Smart Contract powered website by MaDaMa Labs entirely running on the WAVES Blockchain where anyone can own a piece of the blockchain and immortalize it with an image and a link.

We took inspiration from the famous “Million Dollar Homepage” and enriched it with the power of the WAVES Blockchain, Smart Contracts and WAVES Keeper, reaching a strong integration between the WEB and the blockchain in an easy and elegant way.

Follow us on [Twitter](https://twitter.com/MaDaMaLabs) and Join us on [Telegram](https://t.me/MaDaMaLabs)